Dockerized Indico
=================

This is a dockerized indico `Indico <https://docs.getindico.io>`_ instance.
Currently it uses the development version of the flask web server.

Preparation
-----------

- Clone the repository: ``git clone https://gitlab.com/artur-scholz/docker-indico.git``

- Enter the directory: ``cd docker-indico``

- Change the passwords in the files *indico.conf* and *docker-compose.yml* and
  adjust the file content to your server configuration.

- Prepare the postgres database of the container as follows:
    - Bring up the container: ``docker-compose up --build``
    - In a second terminal, find the ID of the running *postgres* container (has postgres in its name): ``docker ps``
    - Bash into this running postgres container using the ID: ``docker exec -it <ID> bash`` and run:
        - ``su - postgres -c 'createuser indico --pwprompt'``
        - ``su - postgres -c 'createdb -O indico indico'``
        - ``su - postgres -c 'psql indico -c "CREATE EXTENSION unaccent; CREATE EXTENSION pg_trgm;"'``
    - Stop the container from the first terminal (Crtl-C)

- Populate the database:
    - Bash into the container: ``docker-compose run indico bash``
    - Execute the following: ``. /opt/indico/.venv/bin/activate && indico db prepare``
    - Exit and stop the container: ``exit``

Running the Server
------------------

When preparation has been completed successfully, the container can be started
and stopped any time.

To start the container, issue: ``docker-compose up``. This runs it in interactive
mode with the log output printed on screen. Stop it with Ctr-C.

To run container in daemon mode, issue: ``docker-compose up -d`

The website is accessible at `http://localhost:80 <http://localhost:80>`_

When running for the first time, a bootstrap will be done to create the superuser.
