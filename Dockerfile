FROM ubuntu:18.04


# Install packages
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --install-recommends \
    software-properties-common \
    libxslt1-dev libxml2-dev libffi-dev libpcre3-dev libyaml-dev \
    libjpeg-turbo8-dev zlib1g-dev \
    uwsgi uwsgi-plugin-python \
  	git \
  	python python-dev python-virtualenv python-setuptools python-pip \
  	nginx \
    wget liblz4-tool \
  	supervisor && \
  	pip install -U pip setuptools && \
    rm -rf /var/lib/apt/lists/*


# Install Texlive
COPY config/texlive.profile /tmp/texlive.profile
RUN cd /tmp && \
  wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
  tar xvzf install-tl-unx.tar.gz && cd install-tl-*/ && \
  mv /tmp/texlive.profile . && ./install-tl --profile texlive.profile


# Install celery,uwsgi and cleanup the cache
RUN pip install uwsgi celery --cache-dir=/tmp/pip_cache && \
    rm -rf /tmp/pip_cache


# Install postgres-dev
RUN apt-get update && apt-get install -y libpq-dev


# Create user indico
RUN useradd -rm -d /opt/indico -s /bin/bash indico
USER indico


# Install indico
RUN virtualenv opt/indico/.venv && \
    . opt/indico/.venv/bin/activate  && \
    pip install indico


# Prepare folders
RUN mkdir /opt/indico/archive && \
    mkdir /opt/indico/assets && \
    mkdir /opt/indico/cache && \
    mkdir /opt/indico/etc && \
    mkdir /opt/indico/log && \
    mkdir /opt/indico/log/nginx && \
    mkdir /opt/indico/log/celery && \
    mkdir /opt/indico/tmp && \
    mkdir /opt/indico/web
COPY config/indico.wsgi /opt/indico/web/


# Setup all the configfiles
USER root
RUN echo "daemon off;" >> /etc/nginx/nginx.conf && rm /etc/nginx/sites-enabled/default
COPY config/nginx.conf /etc/nginx/sites-enabled/
COPY config/supervisor.conf /etc/supervisor/conf.d/
COPY config/indico.conf /etc/indico.conf
COPY config/logging.yaml /etc/logging.yaml


# Create an SSL certificate
RUN mkdir /etc/ssl/indico && chown root:root /etc/ssl/indico/ && \
    chmod 700 /etc/ssl/indico
RUN openssl req -x509 -nodes -newkey rsa:4096 -subj /CN=34.208.82.29 \
    -keyout /etc/ssl/indico/indico.key -out /etc/ssl/indico/indico.crt
